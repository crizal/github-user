package id.belajar.githubuser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.w3c.dom.Text
import id.belajar.githubuser.ListUserAdapter as ListUserAdapter1

class ListUserAdapter(private val listUser: ArrayList<User>):RecyclerView.Adapter<ListUserAdapter1.ListViewHolder>() {
    private lateinit var onItemClickCallback: OnItemClickCallback
    fun setOnItemClickCallback(onItemClickCallback:OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_row_user,parent,false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val (username, name, avatar, company, location, repository, follower, following) = listUser[position]
        holder.tvUsername.text = username
        holder.tvName.text = name
        holder.imgAvatar.setImageResource(avatar)
        holder.tvRepository.text = repository
        holder.tvFollower.text = follower
        holder.tvFollowing.text = following

        holder.itemView.setOnClickListener{onItemClickCallback.onItemClicked(listUser[holder.adapterPosition])}

    }

    override fun getItemCount(): Int = listUser.size

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgAvatar: ImageView = itemView.findViewById(R.id.img_avatar)
        var tvName: TextView = itemView.findViewById(R.id.tv_name)
        var tvUsername: TextView = itemView.findViewById(R.id.tv_username)
        var tvRepository: TextView = itemView.findViewById(R.id.tv_numb_of_repository)
        var tvFollower: TextView = itemView.findViewById(R.id.tv_numb_of_follower)
        var tvFollowing: TextView = itemView.findViewById(R.id.tv_numb_of_following)
    }

    interface OnItemClickCallback{
        fun onItemClicked(data: User)
    }
}